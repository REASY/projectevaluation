﻿using MongoDB.Bson.Serialization.Attributes;

namespace ProjectEvaluation.Data.Entities
{
    [BsonIgnoreExtraElements]
    public class Book : MongoEntity
    {
        public string Title { get; set; }
        public string Publisher { get; set; }
        public string Description { get; set; }
        public string[] Authors { get; set; }
    }
}
