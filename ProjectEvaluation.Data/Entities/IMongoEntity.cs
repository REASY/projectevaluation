﻿using MongoDB.Bson;

namespace ProjectEvaluation.Data.Entities
{
    public interface IMongoEntity
    {
        ObjectId Id { get; set; }
    }
}
