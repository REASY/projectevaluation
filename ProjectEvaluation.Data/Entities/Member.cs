﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace ProjectEvaluation.Data.Entities
{
    public class Member : MongoEntity
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        DateTimeOffset CreatedDateUtc { get; set; }
        DateTimeOffset ChangedDateUtc { get; set; }
        DateTimeOffset LastSignInDateUtc { get; set; }
        public string Password { get; set; }

        public ObjectId LibraryId { get; set; }
        [BsonIgnore]
        public Library Library { get; set; }
    }
}
