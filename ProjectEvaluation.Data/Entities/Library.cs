﻿using System.Collections.Generic;

namespace ProjectEvaluation.Data.Entities
{
    public class Library : MongoEntity
    {
        public string Name { get; set; }
        public Address Address { get; set; }

        public IList<Member> Members { get; set; }
    }
}