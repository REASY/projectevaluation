﻿using MongoDB.Driver;
using ProjectEvaluation.Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectEvaluation.Core.DAL.Interfaces
{
    public interface IEntityService<T> : IDisposable where T : IMongoEntity
    {
        Task Create(T entity);
        Task Delete(string id);
        Task<T> GetById(string id);
        Task Update(T entity);
        Task<IReadOnlyList<T>> GetByFilter(FilterDefinition<T> filter, FindOptions<T, T> findOptions = null);
        Task<IReadOnlyList<T>> GetByStringField(string field, string value, bool strictSearch = false, FindOptions<T, T> findOptions = null);
    }
}
