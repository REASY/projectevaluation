﻿using ProjectEvaluation.Data.Entities;
using System.Threading.Tasks;

namespace ProjectEvaluation.Core.DAL.Interfaces
{
    public interface ILibraryService : IEntityService<Library>
    {
        Task<Library> GetByName(string libName);
        Task<Library> GetLibraryWithoutMembers(string libraryId);
        Task DeleteMember(string libraryId, string memberId);
    }
}
