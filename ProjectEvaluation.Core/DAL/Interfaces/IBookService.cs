﻿using MongoDB.Driver;
using ProjectEvaluation.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEvaluation.Core.DAL.Interfaces
{
    public interface IBookService : IEntityService<Book>
    {
        Task<IReadOnlyList<Book>> GetByTitle(string title, bool strictSearch = false);
        Task<IReadOnlyList<Book>> GetByPublisher(string publisher, bool strictSearch = false);
        Task<IReadOnlyList<Book>> GetByDescription(string description, bool strictSearch = false);
        Task<IReadOnlyList<Book>> GetByAuthor(string author, bool strictSearch = false);
    }
}
