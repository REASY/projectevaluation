﻿using ProjectEvaluation.Data.Entities;
using System.Threading.Tasks;

namespace ProjectEvaluation.Core.DAL.Interfaces
{
    public interface IMemberService : IEntityService<Member>
    {
        Task<Member> GetByEmail(string email);
    }
}
