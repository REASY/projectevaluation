﻿using System;
using MongoDB.Driver;
using ProjectEvaluation.Data.Entities;

namespace ProjectEvaluation.Core.DAL.Impl
{
    public class MongoConnectionHandler<T> where T : IMongoEntity
    {
        public MongoUrl ConnectionString { get; private set; }
        public string DbName { get; private set; }
        public IMongoCollection<T> MongoCollection { get; private set; }

        public MongoConnectionHandler(MongoUrl connectionString, string dbName)
        {
            if (connectionString == null)
                throw new ArgumentNullException("connectionString");
            if (string.IsNullOrEmpty(dbName))
                throw new ArgumentException("is null or empty", "dbName");

            ConnectionString = connectionString;
            DbName = dbName;
            var mongoClient = new MongoClient(connectionString);
            var db = mongoClient.GetDatabase(dbName);

            // Get a reference to the collection object from the Mongo database object
            // The collection name is the type name
            MongoCollection = db.GetCollection<T>(typeof(T).Name);
        }
    }
}
