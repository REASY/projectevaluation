﻿using System;
using System.Threading.Tasks;
using MongoDB.Driver;
using ProjectEvaluation.Data.Entities;
using ProjectEvaluation.Core.DAL.Interfaces;
using MongoDB.Bson;
using ProjectEvaluation.Core.DAL.Exceptions;
using System.Collections.Generic;

namespace ProjectEvaluation.Core.DAL.Impl
{
    public class BookService : EntityService<Book>, IBookService
    {
        public BookService(MongoUrl connectionString, string dbName)
            : base(connectionString, dbName)
        {
        }
        public async Task<IReadOnlyList<Book>> GetByTitle(string title, bool strictSearch = false)
        {
            return await GetByStringField("Title", title, strictSearch);
        }
        public async Task<IReadOnlyList<Book>> GetByPublisher(string publisher, bool strictSearch = false)
        {
            return await GetByStringField("Publisher", publisher, strictSearch);
        }
        public async Task<IReadOnlyList<Book>> GetByDescription(string description, bool strictSearch = false)
        {
            return await GetByStringField("Description", description, strictSearch);
        }
        public async Task<IReadOnlyList<Book>> GetByAuthor(string author, bool strictSearch = false)
        {
            return await GetByStringField("Authors", author, strictSearch);
        }
    }
}
