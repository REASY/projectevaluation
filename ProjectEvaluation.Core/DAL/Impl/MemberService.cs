﻿using System;
using System.Threading.Tasks;
using MongoDB.Driver;
using ProjectEvaluation.Data.Entities;
using ProjectEvaluation.Core.DAL.Interfaces;
using System.Linq;
using MongoDB.Bson;

namespace ProjectEvaluation.Core.DAL.Impl
{
    public class MemberService : EntityService<Member>, IMemberService
    {
        private readonly ILibraryService _libraryService;

        public MemberService(MongoUrl connectionString, string memberDbName, string libraryDbName)
            : base(connectionString, memberDbName)
        {
            _libraryService = new LibraryService(connectionString, libraryDbName);
        }

        public async Task<Member> GetByEmail(string email)
        {
            var res = await GetByStringField("Email", email, true);
            var member =  res.FirstOrDefault();
            if (member != null) {
                member.Library = await _libraryService.GetLibraryWithoutMembers(member.LibraryId.ToString());
            }
            return member;
        }
        public override async Task Delete(string id)
        {
            var member = await GetById(id);
            if (member == null)
                return;
            // Delete member from Member collection
            await base.Delete(id);
            // Also delete member from Library.Members
            await _libraryService.DeleteMember(member.LibraryId.ToString(), id);
        }
    }
}
