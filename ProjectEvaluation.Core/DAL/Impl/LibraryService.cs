﻿using System;
using System.Threading.Tasks;
using MongoDB.Driver;
using ProjectEvaluation.Data.Entities;
using ProjectEvaluation.Core.DAL.Interfaces;
using System.Linq;
using MongoDB.Bson;

namespace ProjectEvaluation.Core.DAL.Impl
{
    public class LibraryService : EntityService<Library>, ILibraryService
    {
        public LibraryService(MongoUrl connectionString, string dbName)
            : base(connectionString, dbName)
        {
        }
        public async Task<Library> GetByName(string libName)
        {
            var libs = await GetByStringField("Name", libName, true);
            return libs.FirstOrDefault();
        }
        public async Task DeleteMember(string libraryId, string memberId)
        {
            var filter = Builders<Library>.Filter.Eq("Id", new ObjectId(libraryId));
            var pullFilter = Builders<Library>.Update.PullFilter("Members", Builders<Member>.Filter.Eq("Id", new ObjectId(memberId)));
            var newLib = await MongoCollection.FindOneAndUpdateAsync(filter, pullFilter);
        }

        public async Task<Library> GetLibraryWithoutMembers(string libraryId)
        {
            return await GetById(libraryId);
        }
    }
}
