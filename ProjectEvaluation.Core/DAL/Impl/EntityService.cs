﻿using MongoDB.Bson;
using MongoDB.Driver;
using ProjectEvaluation.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ProjectEvaluation.Core.DAL.Interfaces;
using MongoDB.Bson.Serialization;
using ProjectEvaluation.Core.DAL.Exceptions;

namespace ProjectEvaluation.Core.DAL.Impl
{
    public abstract class EntityService<T> : IEntityService<T> where T : IMongoEntity
    {
        protected readonly int DUPLICATE_KEY_ERROR_CODE = 11000;

        CancellationTokenSource _cts;
        bool _isDisposed;

        protected MongoConnectionHandler<T> MongoConnectionHandler { get; private set; }
        protected IMongoCollection<T> MongoCollection
        {
            get { return MongoConnectionHandler.MongoCollection; }
        }

        public EntityService(MongoUrl connectionString, string dbName)
        {
            _cts = new CancellationTokenSource();
            MongoConnectionHandler = new MongoConnectionHandler<T>(connectionString, dbName);
        }
        public virtual async Task Create(T entity)
        {
            try { await MongoCollection.InsertOneAsync(entity, new InsertOneOptions() { BypassDocumentValidation = true }, _cts.Token); }
            catch (MongoWriteException ex) {
                if (ex.WriteError != null && ex.WriteError.Code == DUPLICATE_KEY_ERROR_CODE)
                    throw new DuplicateKeyException(entity, ex.Message, ex);
                throw;
            }
        }
        public virtual async Task Delete(string id)
        {
            if (await GetById(id) != null) {
                var filter = Builders<T>.Filter.Eq("Id", new ObjectId(id));
                await MongoConnectionHandler.MongoCollection.DeleteOneAsync(filter, _cts.Token);
            }
            else {
                throw new KeyNotFoundException(string.Format("Entity[{0}] not found", id));
            }
        }
        public virtual async Task<T> GetById(string id)
        {
            var filter = Builders<T>.Filter.Eq("Id", new ObjectId(id));

            var cursor = await MongoCollection.FindAsync(filter, null, _cts.Token);
            return await cursor.FirstOrDefaultAsync();
        }
        public virtual async Task Update(T entity)
        {
            if (await GetById(entity.Id.ToString()) != null) {
                var filter = Builders<T>.Filter.Eq("Id", entity.Id);
                try { await MongoCollection.ReplaceOneAsync(filter, entity, null, _cts.Token); }
                catch (MongoWriteException ex) {
                    if (ex.WriteError != null && ex.WriteError.Code == DUPLICATE_KEY_ERROR_CODE)
                        throw new DuplicateKeyException(entity, ex.Message, ex);
                    throw;
                }
            }
            else {
                throw new KeyNotFoundException(string.Format("Entity[{0}] not found", entity.Id));
            }
        }
        public async Task<IReadOnlyList<T>> GetByFilter(FilterDefinition<T> filter, FindOptions<T, T> findOptions = null)
        {
            var cursor = await MongoCollection.FindAsync(filter, findOptions, _cts.Token);
            return await cursor.ToListAsync();
        }
        public async Task<IReadOnlyList<T>> GetByStringField(string field, string value, bool strictSearch = false, FindOptions<T, T> findOptions = null)
        {
            FilterDefinition<T> filter = !strictSearch ? Builders<T>.Filter.Regex(field, new BsonRegularExpression(value))
                : Builders<T>.Filter.Eq(field, value);
            return await GetByFilter(filter, findOptions);
        }
        public void Dispose()
        {
            if (_isDisposed)
                return;
            _cts.Cancel();

            _isDisposed = true;
        }
    }
}
