﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEvaluation.Core.DAL.Exceptions
{
    [Serializable]
    public class DuplicateKeyException : Exception
    {
        public Exception InnerExpeption { get; private set; }
        public object Object { get; private set; }

        public DuplicateKeyException(object obj, string message, Exception innerExpeption)
            :base(message, innerExpeption)
        {
            Object = obj;
        }
    }
}
