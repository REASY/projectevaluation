﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.IO;

namespace ProjectEvaluation.Core.DAL.Impl.Tests
{
    [TestClass]
    public abstract class MongoDbFunctionalTests
    {
        volatile bool _isCleanedUp;

        protected readonly string _connectionString;
        protected string _dbName;
        protected MongoClient _client;
        protected IMongoDatabase _mongoDb;

        protected abstract void CreateSchema();
        protected abstract void FillData();

        public MongoDbFunctionalTests(string cs)
        {
            if (string.IsNullOrEmpty(cs))
                throw new ArgumentException("is null or empty", "cs");
            _connectionString = cs;
        }
        ~MongoDbFunctionalTests()
        {
            if (!_isCleanedUp) {
                _client.DropDatabase(_dbName);
                Console.WriteLine("IsCleanedUp: {0}", _isCleanedUp);
            }
        }
        [TestInitialize]
        public virtual void TestInitialize()
        {
            _dbName = string.Format("{0}_{1}", this.GetType().Name, Guid.NewGuid());
            try {
                CreateMongoDatabase(_dbName);
                CreateSchema();
                FillData();
            }
            catch(Exception) {
                TestCleanup();
                throw;
            }
        }
        [TestCleanup]
        public virtual void TestCleanup()
        {
            _client.DropDatabase(_dbName);
            _isCleanedUp = true;
        }
        private void CreateMongoDatabase(string _dbName)
        {
            _client = new MongoClient(_connectionString); // );
            _mongoDb = _client.GetDatabase(_dbName);
        }
        protected void ImportJsonFromFile<T>(IMongoCollection<T> collection, string filename)
        {
            using (var streamReader = new StreamReader(filename)) {
                string line;
                while ((line = streamReader.ReadLine()) != null) {
                    using (var jsonReader = new JsonReader(line)) {
                        var context = BsonDeserializationContext.CreateRoot(jsonReader);
                        var document = collection.DocumentSerializer.Deserialize(context);
                        collection.InsertOne(document);
                    }
                }
            }
        }
        protected string GetNotExistingEntityId()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }
    }
}