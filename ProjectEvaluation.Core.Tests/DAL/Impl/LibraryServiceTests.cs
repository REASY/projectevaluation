﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using ProjectEvaluation.Core.DAL.Exceptions;
using ProjectEvaluation.Core.DAL.Impl;
using ProjectEvaluation.Core.Tests;
using ProjectEvaluation.Core.Tests.DAL;
using ProjectEvaluation.Core.Tests.Fixures;
using ProjectEvaluation.Data.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEvaluation.Core.DAL.Impl.Tests
{
    [TestClass]
    public class LibraryServiceTests : MongoDbFunctionalTests
    {
        private readonly string FIXURE_DATA_FILE = @"Fixures\Library.json";
        private readonly string COLLECTION_NAME = @"Libary";
        private readonly string EXISTING_LIBRARY_NAME = "Library of Congress";
        private readonly string NOT_EXISTING_LIBRARY_NAME = "SomeNotExistingLibName";

        public LibraryServiceTests() :
            base(@"mongodb://localhost:27017")
        {

        }
        [TestMethod]
        public async Task T()
        {
            using (var libraryService = new LibraryService(MongoUrl.Create(_connectionString), _dbName)) {
                var library = await libraryService.GetByName(EXISTING_LIBRARY_NAME);

                Assert.IsNotNull(library);
            }
        }
        
        protected override void FillData()
        {
            ImportJsonFromFile(_mongoDb.GetCollection<Library>(COLLECTION_NAME), FIXURE_DATA_FILE);
        }
        protected override void CreateSchema()
        {
            _mongoDb.CreateCollection(COLLECTION_NAME);
        }
    }
}