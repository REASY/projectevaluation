﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using ProjectEvaluation.Core.DAL.Exceptions;
using ProjectEvaluation.Core.DAL.Impl;
using ProjectEvaluation.Core.Tests;
using ProjectEvaluation.Core.Tests.DAL;
using ProjectEvaluation.Core.Tests.Fixures;
using ProjectEvaluation.Data.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEvaluation.Core.DAL.Impl.Tests
{
    [TestClass]
    public class BookServiceTests : MongoDbFunctionalTests
    {
        private readonly string FIXURE_DATA_FILE = @"Fixures\Book.json";
        private readonly string COLLECTION_NAME = @"Book";
        private readonly string EXIST_BOOK_ID = @"5513306a2dfd32ffd580e323";
        public BookServiceTests() :
            base(@"mongodb://localhost:27017")
        {

        }

        [TestMethod]
        public async Task GetById_ExistingBook_ShouldBeOk()
        {
            var exptectedBook = BookFixure.CreateBook_5513306a2dfd32ffd580e323();

            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var book = await bookService.GetById(EXIST_BOOK_ID);

                Helper.AreEqual(exptectedBook, book);
            }
        }
        [TestMethod]
        public async Task GetById_NonExistingBook_ShouldBeOk()
        {
            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var book = await bookService.GetById(GetNotExistingEntityId());
                Assert.IsNull(book);
            }
        }
        [TestMethod]
        public async Task Create_NonExistingBook_ShouldBeOk()
        {
            var expectedNewBook = BookFixure.CreateBook_NewBookForInsertion();

            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                await bookService.Create(expectedNewBook);

                var gotNewBook = await bookService.GetById(expectedNewBook.Id.ToString());
                Helper.AreEqual(expectedNewBook, gotNewBook);
            }
        }
        [TestMethod]
        public async Task Create_ExistingBook_ExpectedDuplicateKeyException()
        {
            var expectedNewBook = BookFixure.CreateBook_5513306a2dfd32ffd580e323();

            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var ex = await MyAssert.ThrowsExceptionAsync<DuplicateKeyException>(async () => {
                    await bookService.Create(expectedNewBook);
                });
                Assert.IsNotNull(ex);
            }
        }
        [TestMethod]
        public async Task Delete_ExistingBook_ShouldBeOk()
        {
            var expectedNewBook = BookFixure.CreateBook_NewBookForInsertion();

            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                await bookService.Create(expectedNewBook);
                Assert.IsNotNull(expectedNewBook.Id);

                await bookService.Delete(expectedNewBook.Id.ToString());
            }
        }
        [TestMethod]
        public async Task Delete_NotExistingBook_ExpectedKeyNotFoundException()
        {
            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var ex = await MyAssert.ThrowsExceptionAsync<KeyNotFoundException>(async () => {
                    await bookService.Delete(GetNotExistingEntityId());
                });
                Assert.IsNotNull(ex);
            }
        }
        [TestMethod]
        public async Task Update_ExistingBook_ShouldBeOk()
        {
            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var book = await bookService.GetById(EXIST_BOOK_ID);
                string newTitle = Guid.NewGuid().ToString();
                book.Title = newTitle;
                await bookService.Update(book);

                book = await bookService.GetById(EXIST_BOOK_ID);
                Assert.AreEqual(newTitle, book.Title);
            }
        }
        [TestMethod]
        public async Task Update_NotExistingBook_ExpectedKeyNotFoundException()
        {
            var newBook = BookFixure.CreateBook_NewBookForInsertion();

            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var ex = await MyAssert.ThrowsExceptionAsync<KeyNotFoundException>(async () => {
                    await bookService.Update(newBook);
                });
                Assert.IsNotNull(ex);
            }
        }
        [TestMethod]
        public async Task GetByTitle_NonStrictSearch_ExistingTitle_ShouldBeOk()
        {
            string title = "Discipline";
            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var books = await bookService.GetByTitle(title);

                Assert.AreNotEqual(0, books.Count);
            }
        }
        [TestMethod]
        public async Task GetByTitle_NonStrictSearch_NotExistingTitle_ShouldBeOk()
        {
            string title = "Discipline123213";

            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var books = await bookService.GetByTitle(title);

                Assert.AreEqual(0, books.Count);
            }
        }
        [TestMethod]
        public async Task GetByTitle_StrictSearch_NotExistingTitle_ShouldBeOk()
        {
            string title = "Discipline";
            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var books = await bookService.GetByTitle(title, true);

                Assert.AreEqual(0, books.Count);
            }
        }
        [TestMethod]
        public async Task GetByDescription_StrictSearch_ExistingTitle_ShouldBeOk()
        {
            var book = BookFixure.CreateBook_TheFifthDiscipline();
            string description = book.Description;
            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var books = await bookService.GetByDescription(description, true);

                Assert.AreEqual(1, books.Count);
                Helper.AreEqual(book, books[0]);
            }
        }
        [TestMethod]
        public async Task GetByDescription_StrictSearch_NotExistingTitle_ShouldBeOk()
        {
            string description = "ASD";
            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var books = await bookService.GetByDescription(description, true);

                Assert.AreEqual(0, books.Count);
            }
        }
        [TestMethod]
        public async Task GetByDescription_NonStrictSearch_ExistingTitle_ShouldBeOk()
        {
            var book = BookFixure.CreateBook_TheFifthDiscipline();
            string description = @"bestselling classic";
            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var books = await bookService.GetByDescription(description, false);

                Assert.AreEqual(1, books.Count);
                Helper.AreEqual(book, books[0]);
            }
        }
        [TestMethod]
        public async Task GetByDescription_NOnStrictSearch_NotExistingTitle_ShouldBeOk()
        {
            string description = "11111";
            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var books = await bookService.GetByDescription(description, false);

                Assert.AreEqual(0, books.Count);
            }
        }
        [TestMethod]
        public async Task GetByAuthor_NonStrictSearch_ExistingAuthor_ShouldBeOk()
        {
            string author = "Senge";
            var book = BookFixure.CreateBook_TheFifthDiscipline();
            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var books = await bookService.GetByAuthor(author, false);

                Assert.AreEqual(1, books.Count);
                Helper.AreEqual(book, books[0]);
            }
        }
        [TestMethod]
        public async Task GetByAuthor_NonStrictSearch_NotExistingAuthor_ShouldBeOk()
        {
            string author = "Senge123123";
            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var books = await bookService.GetByAuthor(author, false);

                Assert.AreEqual(0, books.Count);
            }
        }
        [TestMethod]
        public async Task GetByAuthor_StrictSearch_ExistingAuthor_ShouldBeOk()
        {
            string author = "Peter M. Senge";
            var book = BookFixure.CreateBook_TheFifthDiscipline();
            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var books = await bookService.GetByAuthor(author, true);

                Assert.AreEqual(1, books.Count);
                Helper.AreEqual(book, books[0]);
            }
        }
        [TestMethod]
        public async Task GetByAuthor_StrictSearch_NotExistingAuthor_ShouldBeOk()
        {
            string author = "Peter M. Senge1222";
            using (var bookService = new BookService(MongoUrl.Create(_connectionString), _dbName)) {
                var books = await bookService.GetByAuthor(author, true);

                Assert.AreEqual(0, books.Count);
            }
        }
        protected override void FillData()
        {
            ImportJsonFromFile(_mongoDb.GetCollection<Book>(COLLECTION_NAME), FIXURE_DATA_FILE);
        }
        protected override void CreateSchema()
        {
            _mongoDb.CreateCollection(COLLECTION_NAME);
        }
        
    }
}