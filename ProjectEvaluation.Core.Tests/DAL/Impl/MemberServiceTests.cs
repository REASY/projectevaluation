﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using ProjectEvaluation.Core.DAL.Exceptions;
using ProjectEvaluation.Core.DAL.Impl;
using ProjectEvaluation.Core.Tests;
using ProjectEvaluation.Core.Tests.DAL;
using ProjectEvaluation.Core.Tests.Fixures;
using ProjectEvaluation.Data.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEvaluation.Core.DAL.Impl.Tests
{
    [TestClass]
    public class MemberServiceTests : MongoDbFunctionalTests
    {
        private readonly string FIXURE_DATA_FILE = @"Fixures\Member.json";
        private readonly string COLLECTION_NAME = @"Member";
        private readonly string LIBRARY_COLLECTION_NAME = @"Library";
        private readonly string EXISTING_USERNAME = "Test1";
        private readonly string NOT_EXISTING_USERNAME = "Test11111";

        public MemberServiceTests() :
            base(@"mongodb://localhost:27017")
        {

        }
        [TestMethod]
        public async Task GetByUsername_ExistingUsername_ShouldBeOk()
        {
            using (var userService = new MemberService(MongoUrl.Create(_connectionString), _dbName, LIBRARY_COLLECTION_NAME)) {
                var user = await userService.GetByEmail(EXISTING_USERNAME);

                Assert.IsNotNull(user);
                Assert.AreEqual(EXISTING_USERNAME, user.FirstName);
            }
        }
        [TestMethod]
        public async Task GetByUsername_NotExistingUsername_ShouldBeOk()
        {
            using (var userService = new MemberService(MongoUrl.Create(_connectionString), _dbName, LIBRARY_COLLECTION_NAME)) {
                var user = await userService.GetByEmail(NOT_EXISTING_USERNAME);

                Assert.IsNull(user);
            }
        }
        [TestMethod]
        public async Task Create_NotExistingUser_ShouldBeOk()
        {
            var newUser = UserFixure.CreateUser_NewUserForInsertion();
            using (var userService = new MemberService(MongoUrl.Create(_connectionString), _dbName, LIBRARY_COLLECTION_NAME)) {
                await userService.Create(newUser);
                Assert.AreNotEqual(ObjectId.Empty, newUser.Id);

                var got = await userService.GetById(newUser.Id.ToString());
                Assert.IsNotNull(got);
                Helper.AreEqual(newUser, got);
            }
        }
        protected override void FillData()
        {
            ImportJsonFromFile(_mongoDb.GetCollection<Member>(COLLECTION_NAME), FIXURE_DATA_FILE);
        }
        protected override void CreateSchema()
        {
            _mongoDb.CreateCollection(COLLECTION_NAME);
        }
    }
}