﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectEvaluation.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEvaluation.Core.Tests.DAL
{
    internal class Helper
    {
        public static void AreEqual(Book expected, Book got)
        {
            Assert.AreEqual(expected.Title, got.Title);
            Assert.AreEqual(expected.Publisher, got.Publisher);
            Assert.AreEqual(expected.Description, got.Description);
            CollectionAssert.AreEqual(expected.Authors, got.Authors);
        }
        public static void AreEqual(Member expected, Member got)
        {
            Assert.AreEqual(expected.FirstName, got.FirstName);
            Assert.AreEqual(expected.Password, got.Password);
        }
        public static void AreEqual(Library expected, Library got)
        {
            Assert.AreEqual(expected.Name, got.Name);
            Assert.AreEqual(expected.Address, got.Address);
            if (expected.Members != null) {
                Assert.IsNotNull(got.Members);
                Assert.AreEqual(expected.Members.Count, got.Members.Count);
                var expectedMembers = expected.Members.OrderBy(x => x.Id).ToArray();
                var gotMembers = got.Members.OrderBy(x => x.Id).ToArray();

                for (int i = 0; i < expectedMembers.Length; i++)
                    AreEqual(expectedMembers[i], gotMembers[i]);
            }
        }
        public static void AreEqual(Address expected, Address got)
        {
            if (expected != null && got != null) {
                Assert.AreEqual(expected.AddressLine1, got.AddressLine1);
                Assert.AreEqual(expected.AddressLine2, got.AddressLine2);
                Assert.AreEqual(expected.City, got.City);
                Assert.AreEqual(expected.Coutry, got.Coutry);
                Assert.AreEqual(expected.PostalCode, got.PostalCode);
                Assert.AreEqual(expected.State, got.State);
            }
            else
                Assert.Fail();
        }
    }
}
