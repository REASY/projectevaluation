﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEvaluation.Core.Tests
{
    public static class MyAssert
    {
        public static T Throws<T>(Action func) where T : Exception
        {
            T retT = default(T);
            var exceptionThrown = false;
            try {
                func.Invoke();
            } catch (T ex) {
                exceptionThrown = true;
                retT = ex;
            }

            if (!exceptionThrown) {
                throw new AssertFailedException(
                    string.Format("An exception of type {0} was expected, but not thrown", typeof(T))
                    );
            }
            return retT;
        }
        public static async Task<Exception> ThrowsExceptionAsync<TException>(Func<Task> code) where TException : Exception
        {
            try {
                await code();
            } catch (TException ex) {
                if (ex.GetType() == typeof(TException))
                    return ex;
                throw new AssertFailedException(string.Format("An exception of type '{0}' was expected, but got '{1}'", typeof(TException), ex.GetType()));
            }
            throw new AssertFailedException(string.Format("An exception of type '{0}' was expected, but not thrown", typeof(TException)));
        }
    }
}
