﻿using MongoDB.Bson.Serialization;
using ProjectEvaluation.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEvaluation.Core.Tests.Fixures
{
    internal class BookFixure
    {
        const string BOOK_5513306a2dfd32ffd580e323 = @"{""_id"" : ObjectId(""5513306a2dfd32ffd580e323""), ""Title"":""The Ecclesiastical Organisation of the Church of the East, 1318-1913"",""Publisher"":""Peeters Publishers"",""Description"":""This careful and scholarly study assembles and discusses the available evidence for the ecllesiastical organisation of the Church of the East (the so-called Nestorian church) in the Middle East between the fourteenth and twentieth centuries. The author has built on the work of the late J.M. Fiey, but has covered a wider geographical area and used a much wider range of sources. Besides drawing on the memoirs of European and American missionaries and other literary sources, the author has consulted a large number of manuscript catalogues, many of which are only accessible in Arabic sources, and has analysed the evidence of more than 2.500 East Syrian manuscript colophons to establish the dioceses of the Church of the East at different periods, to identify its ecclesiastical elites (patriarchs, bishops, priests, deacons and scribes), and to analyse the rivalry between the churchs traditionalist and Catholic wings after the schism of 1552. The study contains a number of detailed maps, which localise hundreds of East Syrian villages in Kurdistan, and will be an indispensable reference tool for scholars of the Church of the East."",""Authors"":[""David Wilmshurst""]}";
        const string NEW_BOOK_FOR_INSERTION = @"{""Title"":""The Origins of Music"",""Publisher"":""MIT Press"",""Description"":""The book can be viewed as representing the birth of evolutionary biomusicology."",""Authors"":[""Nils Lennart Wallin"", ""Björn Merker""]}";
        const string THE_FIFTH_DISCIPLINE = @"{""_id"" : ObjectId(""5513306a2dfd32ffd580e324""), ""Title"":""The Fifth Discipline"",""Publisher"":""Crown Business"",""Description"":""Completely Updated and Revised This revised edition of Peter Senge’s bestselling classic, The Fifth Discipline, is based on fifteen years of experience in putting the book’s ideas into practice. As Senge makes clear, in the long run the only sustainable competitive advantage is your organization’s ability to learn faster than the competition. The leadership stories in the book demonstrate the many ways that the core ideas in The Fifth Discipline, many of which seemed radical when first published in 1990, have become deeply integrated into people’s ways of seeing the world and their managerial practices. In The Fifth Discipline, Senge describes how companies can rid themselves of the learning “disabilities” that threaten their productivity and success by adopting the strategies of learning organizations—ones in which new and expansive patterns of thinking are nurtured, collective aspiration is set free, and people are continually learning how to create results they truly desire. The updated and revised Currency edition of this business classic contains over one hundred pages of new material based on interviews with dozens of practitioners at companies like BP, Unilever, Intel, Ford, HP, Saudi Aramco, and organizations like Roca, Oxfam, and The World Bank. It features a new Foreword about the success Peter Senge has achieved with learning organizations since the book’s inception, as well as new chapters on Impetus (getting started), Strategies, Leaders’ New Work, Systems Citizens, and Frontiers for the Future. Mastering the disciplines Senge outlines in the book will: • Reignite the spark of genuine learning driven by people focused on what truly matters to them • Bridge teamwork into macro-creativity • Free you of confining assumptions and mindsets • Teach you to see the forest and the trees • End the struggle between work and personal time From the Trade Paperback edition."",""Authors"":[""Peter M. Senge""]}";

        public static Book CreateBook_5513306a2dfd32ffd580e323()
        {
            return BsonSerializer.Deserialize<Book>(BOOK_5513306a2dfd32ffd580e323);
        }
        public static Book CreateBook_NewBookForInsertion()
        {
            return BsonSerializer.Deserialize<Book>(NEW_BOOK_FOR_INSERTION);
        }
        public static Book CreateBook_TheFifthDiscipline()
        {
            return BsonSerializer.Deserialize<Book>(THE_FIFTH_DISCIPLINE);
        }
    }
}
