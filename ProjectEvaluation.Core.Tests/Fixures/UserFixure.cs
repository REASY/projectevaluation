﻿using MongoDB.Bson.Serialization;
using ProjectEvaluation.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEvaluation.Core.Tests.Fixures
{
    internal class UserFixure
    {
        const string NEW_USER_FOR_INSERTION =  @"{""Username"":""Test555"", ""Password"":""Test555""}";

        public static Member CreateUser_NewUserForInsertion()
        {
            return BsonSerializer.Deserialize<Member>(NEW_USER_FOR_INSERTION);
        }
    }
}
