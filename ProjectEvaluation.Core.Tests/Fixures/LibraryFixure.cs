﻿using MongoDB.Bson.Serialization;
using ProjectEvaluation.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEvaluation.Core.Tests.Fixures
{
    internal class LibraryFixure
    {
        const string LIBRARY_OF_CONGRESS = @"{ ""Name"": ""Library of Congress"", ""Address"": { ""AddressLine1"": ""101 Independence Ave SE"", ""City"": ""Washington"", ""State"": ""DC"", ""PostalCode"": ""20540"", ""Coutry"": ""USA"" }, ""Members"": [ ] }";
        const string BODLEIAN_LIBRARY = @"{ ""Name"": ""Bodleian Library"", ""Address"": { ""AddressLine1"": ""Broad St"", ""City"": ""Oxford"", ""State"": """", ""PostalCode"": ""OX1 3BG"", ""Coutry"": ""USA"" }, ""Members"": [ ] }";

        public static Library CreateLibrary_LibraryOfCongress()
        {
            return BsonSerializer.Deserialize<Library>(LIBRARY_OF_CONGRESS);
        }
        public static Library CreateLibrary_BodleianLibrary()
        {
            return BsonSerializer.Deserialize<Library>(BODLEIAN_LIBRARY);
        }
    }
}
